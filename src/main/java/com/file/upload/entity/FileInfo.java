package com.file.upload.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * The Class FileInfo.
 *
 * @author Parmeshwar Munde
 * @version 1.0
 * @since 23 Feb, 2022
 */
@Entity

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Data

/**
 * Instantiates a new file info.
 *
 * @param id the id
 * @param name the name
 * @param uri the uri
 */
@AllArgsConstructor

/**
 * Instantiates a new file info.
 */
@NoArgsConstructor
@ToString
public class FileInfo {
    
    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    /** The name. */
    private String name;
    
    /** The uri. */
    private String uri;


    /**
     * Instantiates a new file info.
     *
     * @param name the name
     * @param uri the uri
     */
    public FileInfo(String name, String uri) {
        this.name=name;
        this.uri=uri;
    }

}
