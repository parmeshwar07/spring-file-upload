package com.file.upload.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.file.upload.entity.FileInfo;

@Repository
public interface FileUploadRepository extends JpaRepository<FileInfo, Long>{

}
