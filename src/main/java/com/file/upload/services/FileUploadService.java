package com.file.upload.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.file.upload.entity.FileInfo;
import com.file.upload.repository.FileUploadRepository;

/**
 * The Class FileUploadService.
 *
 * @author Parmeshwar Munde
 * @version 1.0
 * @since 23 Feb, 2022
 */
@Service
public class FileUploadService {

    /** The upload repository. */
    @Autowired
    private FileUploadRepository uploadRepository;

    /** The root. */
    private final Path root = Paths.get("/home/maverick-18/Pictures/uploads");

    /**
     * Init.
     */
    @PostConstruct
    public void init() {
        try {

            Files.createDirectories(root);
        } catch (IOException e) {
            e.getMessage();
            System.out.println("directory not created");
        }
    }

    /**
     * Save.
     *
     * @param file the file
     */
    public void save(MultipartFile file) {
        try {

            if (!Files.exists(root)) {
                init();
            }
            Files.copy(file.getInputStream(), this.root.resolve(file.getOriginalFilename()));

            FileInfo fileInfo = new FileInfo();
            fileInfo.setName(file.getOriginalFilename());
            fileInfo.setUri(this.root + File.separator+file.getOriginalFilename());
            uploadRepository.save(fileInfo);
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
    }

    public List<FileInfo> getAllFiles() {
        
        return uploadRepository.findAll();
    }

}
