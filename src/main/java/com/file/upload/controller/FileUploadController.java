package com.file.upload.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.file.upload.entity.FileInfo;
import com.file.upload.models.ResponseMessage;
import com.file.upload.services.FileUploadService;

/**
 * The Class FileUploadController.
 *
 * @author Parmeshwar Munde
 * @version 1.0
 * @since 23 Feb, 2022
 */
@RestController
public class FileUploadController {

    /** The file upload service. */
    @Autowired
    FileUploadService fileUploadService;

    /**
     * Upload file.
     *
     * @param file the file
     * @return the response entity
     */
    @PostMapping("/upload")
    public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file) {
        String message = "";
        try {
            fileUploadService.save(file);

            message = "Uploaded the file successfully: " + file.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
        } catch (Exception e) {
            message = "Could not upload the file: " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResponseMessage(message));
        }
    }
    
    @GetMapping("/files")
    public ResponseEntity<ResponseMessage> getAllFiles(){
        String message="";
        
        try {
            List<FileInfo> files=fileUploadService.getAllFiles(); 
            files.forEach(file->System.out.println(file));
            message="Files Are loaded";
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
        } catch (Exception e) {
            message="Error: Files not loaded.";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
        }
        
    }
    
    @GetMapping("/get-file/{fileName}")
    public ResponseEntity<?> getFileByName(@PathVariable("fileName") String fileName){
        List<FileInfo> files=fileUploadService.getAllFiles();
        Optional<FileInfo> findFirst = files.stream().filter(file->file.getName().compareTo(fileName)==0).findFirst();
        
        if(findFirst.isPresent()) {
            System.out.println(findFirst.get().getName());
            return ResponseEntity.ok(findFirst.get());
            
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
    
    
    
    
    
    
    
    
    
    
  

}
