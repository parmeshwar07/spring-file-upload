package com.file.upload.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Data

/**
 * Instantiates a new response message.
 */
@NoArgsConstructor

/**
 * Instantiates a new response message.
 *
 * @param message the message
 */
@AllArgsConstructor
public class ResponseMessage {

    /** The message. */
    private String message;
}
