package com.file.upload.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Data

/**
 * Instantiates a new file info DTO.
 */
@NoArgsConstructor

/**
 * Instantiates a new file info DTO.
 *
 * @param name the name
 * @param uri the uri
 */
@AllArgsConstructor
public class FileInfoDTO {

    /** The name. */
    private String name;
    
    /** The uri. */
    private String uri;
}
